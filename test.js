const auctionScrapper = require("./auctionScrapper");
const fs= require("fs");

String.prototype.cut= function(i0, i1) {
    return this.substring(0, i0)+this.substring(i1);
}

async function testUrlConversion(){
  auctionScrapper.scrapeAuctions(fs.readFileSync("./testPage.html"));

}
const convertToTimeUrl=function(url){
  let startIndex=url.indexOf("&order");
  if(startIndex<0)startIndex=url.indexOf("?order");
  if(startIndex>0){
    let endIndex=url.indexOf("&",startIndex+2);
    if(endIndex<0)endIndex=url.length-1;
    url=url.cut(startIndex,endIndex);
  }
  if(url.indexOf("?")<0)url+="?order=n"
  else url+="&order=n";
  return url;
}

function testUrl(url){
  console.log(url);
  console.log(convertToTimeUrl(url));
}

//testUrl("https://allegro.pl/kategoria/motoryzacja-samochody-149?order=d");
//testUrl("https://allegro.pl/kategoria/osobowe-ford-4036?string=auto&order=n&bmatch=baseline-adv-1-2-0725");
testUrlConversion();
