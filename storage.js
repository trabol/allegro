
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('db.json')
const db = low(adapter)


const SEARCHES="searches";
const USER="user";
const COUNT="count";
// Set some defaults (required if your JSON file is empty)
db.defaults({ searches:[], user: {}, count: 0 })
  .write()
/*
search:{
url:,
auctions:{}

}
*/
var storeSearch= function(search){
  search.id=db.get(COUNT).value();
  db.get(SEARCHES)
  .push(search)
  .write();
  let count =db.get(COUNT).value();
  count++;
  db.set(COUNT,count).write();
}

var getSearch =  function(url){
  return db.get(SEARCHES)
    .find({url:url})
    .value();
}

const getSearchById=function(id){
  return db.get(SEARCHES)
  .find({id:id})
  .value();
}

const  deleteSearchById=function(id){
  return db.get(SEARCHES)
  .remove({id:Number(id)})
  .write();
}

var setSearches = function(searches){
  db.set(SEARCHES,searches).write();
}

var removeSearch =  function(url){
   return db.get(SEARCHES)
    .remove({url:url})
    .write();
}

var getAllSearches = function(){
  return db.get(SEARCHES)
    .value();
}

const saveUser = function(user){
  db.set(USER,user).write();
}
const getUser = function(){
  return db.get(USER).value();
}

const updateSearch=function(search){
  db.get(SEARCHES)
  .find({url:search.url})
  .assign(search)
  .write();
}

module.exports ={
  getSearch:getSearch,
  storeSearch:storeSearch,
  removeSearch:removeSearch,
  getAllSearches:getAllSearches,
  saveUser:saveUser,
  getUser:getUser,
  setSearches:setSearches,
  updateSearch:updateSearch,
  getSearchById:getSearchById,
  deleteSearchById:deleteSearchById
}
