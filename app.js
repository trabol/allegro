const storage = require("./storage");
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = express();
const config = require("./config")
const client = require("./client");
const mailer = require("./mailService")
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get("/test",async function(req,res,next){
  res.send(await client.getTest());
});
app.post("/search",async function(req,res,next){
  let items = await client.getAuctionsForUrl(req.body.url,1);
  let search={
    url:req.body.url,
    items:{
      latestPromoted:items.promoted,
      latestRegular:items.latestRegular
    }  }
  storage.storeSearch(search);
  res.redirect("/");
});
app.post("/search/delete",function(req,res,next){
  console.log(req.body.id);
  storage.deleteSearchById(req.body.id);
  res.redirect("/");
});

app.get("/",async function(req,res,next){
  res.render("site.ejs",{
    user:storage.getUser(),
    searches:storage.getAllSearches()})

})




//console.log(apiRouter.stack);



// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  //res.locals.message = err.message;
  //es.locals.error = req.app.get('env') === 'development' ? err : null;

  // render the error page
  //res.status(err.status || 500);
  //development
  //res.render("error");
  //production
  console.log(err);
  res.status(err.status||500).json({error:err});
});

var debug = require('debug')('sql:server');
var http = require('http');

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

var server = http.createServer(app);
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);
console.log("ready");

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

async function getLatest(){
  let searches = storage.getAllSearches();
  console.log("getting latest");
  for(let i=0;i<searches.length;i++){
    let items = await client.getAuctionsForUrl(searches[i].url,1);
    searches[i].items={
      latestPromoted:items.promoted,
      latestRegular:items.regular
    };
    if(searches[i].items.latestRegular.length===0){
      for(let retry=2;retry<=5;retry++){
        let items = await client.getAuctionsForUrl(searches[i].url,retry);
        if(items.regular.length>0){
          searches[i].items.latestRegular=items.regular;
          break;
        }
      }
    }
  }
  storage.setSearches(searches);
  console.log("hasLatest")
  setTimeout(checkForUpdates,1000);
}

async function checkForUpdates(){
  console.log("getting new updates");
  let searches = storage.getAllSearches();
  let promises=[];
  console.log(searches.length);
  for(let i=0;i<searches.length;i++){
    promises.push(checkForSearches(searches[i]));
  }
  await Promise.all(promises);
  storage.setSearches(searches);
  setTimeout(arguments.callee,60000);
}

function existed(latest,item){
  for(let i=0;i<latest.length;i++){
    if(latest[i].url===item.url){
      return true;
    }
  }
  return false;
}

async function checkForSearches(search){
  let firstItems={
    latestPromoted:[],
    latestRegular:[]
  }
  let areDonePromoted=false;
  let itemsToAdd=[];
  let areDoneRegular=false;
  let hasFirstPromoted=false;
  let hasFirstRegular=false
  let retries=1;
  while(retries<=5){
    let portion = await client.getAuctionsForUrl(search.url,retries);

    if(portion.promoted.length>0){
      if(!hasFirstPromoted){
        console.log("adding promoted");
        firstItems.latestPromoted=portion.promoted;
        hasFirstPromoted=true;
      }
      if(!areDonePromoted){
        for(let i=0;i<portion.promoted.length;i++){
          if(existed(search.items.latestPromoted,portion.promoted[i])){
            areDonePromoted=true;
            break;
          }
          itemsToAdd.push(portion.promoted[i]);
        }
      }
    }
    if(portion.regular.length>0){
      if(!hasFirstRegular){
        console.log("adding regular");
        firstItems.latestRegular=portion.regular;
        hasFirstRegular=true;
      }
      if(!areDoneRegular){
        for(let i=0;i<portion.regular.length;i++){
          if(existed(search.items.latestRegular,portion.regular[i])){
            areDoneRegular=true;
            break;
          }
          itemsToAdd.push(portion.regular[i]);
        }
      }
    }
    if(areDoneRegular&&areDonePromoted)break;
    retries++;
  }
  search.items=firstItems;
  storage.updateSearch(search);
  console.log("ItemsToAdd");
  console.log(itemsToAdd.length);
  if(itemsToAdd.length>0&&false){
    let text ="<p>Twoje nowe aukcje: </p>";
    for(let i=0;i<itemsToAdd.length;i++){
      text+='<p><a href="'+itemsToAdd[i].url+'">'+itemsToAdd[i].url+'</a></p>'
    }
    mailer.sendMail(text);
  }
}

getLatest();
module.exports = app;
