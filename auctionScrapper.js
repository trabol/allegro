const cheerio = require("cheerio");
module.exports.scrapeAuctions=function(page){
  let $ = cheerio.load(page);
  let items={
    promoted:[],
    regular:[]
  }
  $("article").each(function(index,element){
    if($(element).parent().prev().text()==="Oferty sponsorowaneSprzedawca tej oferty korzysta z usługi Allegro Ads. Kliknij tutaj, by dowiedzieć się więcej na ten temat."){
      return;
    }else if($(element).parent().prev().text()==="Lista promowanych ofert"){
      let url=$(element).children().children().children().eq(1).children().children().children().attr("href");
      if(url)
        items.promoted.push({url:url});
    }else{
      let url=$(element).children().children().children().eq(1).children().children().children().attr("href");
      if(url)
        items.regular.push({url:url});
    }
  });
  return items;
}
