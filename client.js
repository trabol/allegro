const axios = require('axios');
const client = axios.create({validateStatus:(status)=>true});
const config = require('./config')
const storage = require("./storage");
const auctionScrapper = require("./auctionScrapper");
const timeout = ms => new Promise(res => setTimeout(res, ms))
const fetch = require("node-fetch");
const fs=require("fs");
String.prototype.cut= function(i0, i1) {
    return this.substring(0, i0)+this.substring(i1);
}

const get = async function(url,headers){
  if(!headers)headers={};
  let retries =0;
  let response={};
  await timeout(20);
   response = await fetch(url);
   let text=await response.text();
  return text;
}

const post = async function(url,body,headers){
  let retries =0;
  let response={};
  while(retries<3){
      response = await client.post(url,body,{headers:headers});
    if(response.status===200||response.status===302)return response.data;
    await refreshToken();
    retries++;
  }
  throw new Error("CANNOT LOGIN");
}

const convertToTimeUrl=function(url){//time sort
  let startIndex=url.indexOf("&order");
  if(startIndex<0)startIndex=url.indexOf("?order");
  if(startIndex>0){
    let endIndex=url.indexOf("&",startIndex+2);
    if(endIndex<0)endIndex=url.length-1;
    url=url.cut(startIndex,endIndex+1);
  }
  if(url.indexOf("?")<0)url+="?order=n"
  else url+="&order=n";
  return url;
}

const getAuctionsForUrl=async function(url,page){
  let newUrl = convertToTimeUrl(url);//convertUrlToAPIUrl(url,page);
  console.log("NEW URL");
  console.log(newUrl);
  let data = await get(newUrl,{
  });
  let items = auctionScrapper.scrapeAuctions(data);
  return items;
}




module.exports={
  getAuctionsForUrl:getAuctionsForUrl
}
